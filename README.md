<div align="center">
<img src="data/icons/hicolor/scalable/apps/org.gnome.Decibels.svg" height="64">

# Decibels

Play audio files.

![The main view of Decibels.](./data/screenshots/screenshot-1.png)

![Decibels in dark mode and on a mobile.](./data/screenshots/screenshot-2.png)

</div>

## Installing

### Flathub

Decibels is available to download on
[Flathub](https://flathub.org/apps/details/org.gnome.Decibels).

<a href="https://flathub.org/apps/details/org.gnome.Decibels" title="Download Decibels on Flathub">
  <picture>
    <source media="(prefers-color-scheme: dark)" srcset="https://flathub.org/api/badge?svg&locale=en&light">
    <source media="(prefers-color-scheme: light)" srcset="https://flathub.org/api/badge?svg&locale=en">
    <img alt="Download Decibels on Flathub" src="https://flathub.org/api/badge?svg&locale=en">
  </picture>
</a>

### From gnome-nightly

You can install the latest Nightly Flatpak from the [gnome-nightly] repo.

```
flatpak remote-add --user gnome-nightly https://nightly.gnome.org/gnome-nightly.flatpakrepo
flatpak install --user gnome-nightly org.gnome.Decibels.Devel
```

### From source

Open the project in GNOME Builder and click "Build & Run Project".

## Code of conduct

Decibels follows the [GNOME Code of Conduct](https://conduct.gnome.org/).

- **Be friendly.** Use welcoming and inclusive language.
- **Be empathetic.** Be respectful of differing viewpoints and experiences.
- **Be respectful.** When we disagree, we do so in a polite and constructive
  manner.
- **Be considerate.** Remember that decisions are often a difficult choice
  between competing priorities.
- **Be patient and generous.** If someone asks for help it is because they need
  it.
- **Try to be concise.** Read the discussion before commenting.

## Credits

- [Audio Player app: issue by Allan Day](https://gitlab.gnome.org/Teams/Design/app-mockups/-/issues/96)
- [Audio Player mockups](https://gitlab.gnome.org/Teams/Design/app-mockups/-/blob/master/audio-player/audio-player.png?ref_type=heads)

[gnome-nightly]: https://nightly.gnome.org
